package com.example.veriko.eurodolar;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends Activity implements View.OnClickListener{

    EditText etEuros;
    Button btCalcular, btReanudar;
    RadioButton dolaresEEUU, dolaresCanada;
    char dolares='E';

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        ESTAS DOS LINEAS SALEN SIEMPRE
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        HAGO QUE RECONOZCA LOS CONTROLES POR EL ID QUE LE HE PUESTO
        etEuros=(EditText)findViewById(R.id.etEuros);
        btCalcular=(Button)findViewById(R.id.btCalcular);
        btReanudar=(Button)findViewById(R.id.btReanudar);
        dolaresCanada=(RadioButton)findViewById(R.id.dolaresCanada);
        dolaresEEUU=(RadioButton)findViewById(R.id.dolaresEEUU);
//        SETEO EL VALOR POR DEFECTO
        dolaresEEUU.setChecked(true);

        btCalcular.setOnClickListener(this);
        btReanudar.setOnClickListener(this);
        dolaresEEUU.setOnClickListener(this);
        dolaresCanada.setOnClickListener(this);
    }

    @Override
    public void onClick(View vista) {

        if (vista.getId()==R.id.dolaresEEUU){
            dolares='E';
        }else if (vista.getId()==R.id.dolaresCanada){
            dolares='C';
        }

        if (vista.getId()==R.id.btCalcular){

 //        HACEMOS EL EVENTO
            double eurosIntroducidos = Double.parseDouble(etEuros.getText().toString());
 //        CREAMOS EL INTENT PARA CONECTARNOS CON LA OTRA VISTA
            Intent i = new Intent(this, ResultadoConversion.class);
 //        HACEMOS EL BUNDLE PARA PASARLE LOS DATOS
            Bundle datos = new Bundle();
            datos.putDouble("EUROS", eurosIntroducidos);
            datos.putChar("DOLARES", dolares);
//          ASOCIAMOS EL BUNDLE AL INTENT
            i.putExtras(datos);
//          LANZAMOS LA ACTIVIDAD Y PASAMOS EL INTENT
            startActivity(i);

        }

        if (vista.getId()==R.id.btReanudar){

//            super.onResume();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }


    }
}
