package com.example.veriko.eurodolar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Veriko on 24/04/2017.
 */

class ResultadoConversion extends Activity{

    TextView resultado;
    ImageView imagenResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        resultado=(TextView)findViewById(R.id.resultado);
        imagenResultado=(ImageView)findViewById(R.id.imagenResultado);

//        RECOJO LOS DATOS DEL INTENT QUE SE ENVIA DESDE EL MAIN
        Bundle datos=getIntent().getExtras();
        double euros=datos.getDouble("EUROS");
        char dolares=datos.getChar("DOLARES");

//        CALCULAMOS LA CONVERSION

        double conversion;

        if (dolares == 'E'){
            conversion=euros*1.08625;
            imagenResultado.setImageResource(R.drawable.eeuu);
            resultado.setText("Dolares de EEUU:" + conversion);
        } else if (dolares == 'C'){
            conversion=euros*1.46062596;
            imagenResultado.setImageResource(R.drawable.canada);
            resultado.setText("Dolares canadienses: " + conversion);
        }

    }
}
